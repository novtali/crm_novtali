<?php include_once("config/settings.php"); 

$valid = false;

if(isset($_POST['logout'])) {
    session_start();
    session_unset();
    session_destroy();
}

if(isset($_POST['login'])) {

    $user = new User();

    if($user->login($_POST['email'], $_POST['password'])) {
        session_start();
        $_SESSION['user'] = $user;
        $valid = true;
    }
}

if(isset($_SESSION['user'])) {
    header("Location: /");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Novtali CRM - Connexion</title>
    <?= $link_css ?>

</head>

<body>

    <div class="container-login">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-12 col-md-9">
                <div class="card shadow-sm my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-form">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Connexion</h1>
                                    </div>
                                    <form class="user" method="POST">
                                        <div class="form-group">
                                            <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Adresse email" name="email" value="<?= (isset($_POST['login'])) ? $_POST['email'] : '' ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Mot de passe" name="password" required>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block" name="login">Se connecter</a>
                                        </div>
                                        <?php if(!$valid && isset($_POST['login'])): ?>
                                        <div class="alert alert-danger" role="alert">
                                            Adresse mail et/ou mot de passe incorrect !
                                        </div>
                                        <?php endif; ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>