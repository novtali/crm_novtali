<?php include_once("../config/settings.php"); 

session_start();

if(!isset($_SESSION['user'])) {
    header("Location: /login");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Novtali CRM - Prospects</title>
    <?= $link_css ?>

</head>

<body id="page-top">
    
    <div id="wrapper">

        <?php include_once('sidebar.php'); ?>

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <?php include_once('navbar.php'); ?>

                

            </div>

            <?php include_once('footer.php'); ?>

        </div>

    </div>

    <?= $link_js ?>

</body>

</html>