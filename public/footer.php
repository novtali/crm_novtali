<footer class="sticky-footer bg-white">
<div class="container my-auto">
    <div class="copyright text-center my-auto">
    <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - Développé par
        <b><a href="https://novtali.com" target="_blank">novtali</a></b>
    </span>
    </div>
</div>
</footer>