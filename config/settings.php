<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Define root
define('_APP_DIR_', $_SERVER['DOCUMENT_ROOT']);

// Link assets
$link_css = file_get_contents(_APP_DIR_.'/config/link_css.php');  
$link_js = file_get_contents(_APP_DIR_.'/config/link_js.php');  

// Add class
include_once('autoload.php');

