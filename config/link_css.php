<link rel="icon" type="image/png" href="../assets/img/logo/novtali_transparent.png" />

<!-- STYLE -->
<link rel="stylesheet" type="text/css" href="../assets/fontawesome/css/all.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../assets/css/ruang-admin.min.css">
<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
<!-- /STYLE -->