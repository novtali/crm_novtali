<?php

include_once(_APP_DIR_.'/config/db.php');

class DB {

    private $host;
    private $db;
    private $port;
	private $login;
	private $pass;
	private $connec;

	public function __construct(){
		$this->login = DB_LOGIN;
		$this->pass = DB_PASS;
		$this->db = DB_NAME;
        $this->port = DB_PORT;
        $this->host = DB_HOST;
		$this->connexion();
	}

	private function connexion() {

		try
		{
	        $bdd = new PDO('mysql:host='.$this->host.';dbname='.$this->db.';charset=utf8;port='.$this->port, $this->login, $this->pass);
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			$bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
			$this->connec = $bdd;
		}
		catch (PDOException $e)
		{
			$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
			die($msg);
		}
	}

	public function customQuery($sql,Array $cond = null) {

        try
		{
            $stmt = $this->connec->prepare($sql);

            if($cond){
                foreach ($cond as $v) {
                    $stmt->bindParam($v[0],$v[1],$v[2]);
                }
            }

            $stmt->execute();
            return $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt=NULL;
            
        } catch (PDOException $e) {
            $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
			die($msg);
        }
	}

}